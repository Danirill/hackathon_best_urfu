class Custumer:
    def __init__(self):
        self.CHANGES = []
        self.BROKEN = False
        self.SUBS_ID = []
        self.TARIFF_ID = []
        self.CHARGE = []
        self.SUPPORT_4G = []
        self.SUPPORT_3G = []
        self.LTE_TRAFF_FLAG = []
        self.SMS_IN_CNT = []
        self.SMS_OUT_CNT = []
        self.CALLS_OUT_CNT = []
        self.CALLS_IN_CNT = []
        self.LIFE_TIME = []
        self.DURATION_IN_MIN = []
        self.DURATION_OUT_MIN = []
        self.DATA_TRAFFIC_MB = []
        self.SESSIONS_CNT = []
        self.RECHARGE = []
        self.RECHARGE_CNT = []
        self.DATA_CNT = 0
        self.SELECTION = []

    def Update(self, data, flag):
        self.TARIFF_ID.append(data[2])
        self.CHARGE.append(data[3])
        self.SUPPORT_4G.append(data[4])
        self.SUPPORT_3G.append(data[5])
        self.LTE_TRAFF_FLAG.append(data[6])
        self.SMS_IN_CNT.append(data[7])
        self.SMS_OUT_CNT.append(data[8])
        self.CALLS_IN_CNT.append(data[9])
        self.CALLS_OUT_CNT.append(data[10])
        self.LIFE_TIME.append(data[11])
        self.DURATION_IN_MIN.append(data[12])
        self.DURATION_OUT_MIN.append(data[13])
        self.DATA_TRAFFIC_MB.append(data[14])
        self.SESSIONS_CNT.append(data[15])
        self.RECHARGE.append(data[16])
        self.RECHARGE_CNT.append(data[17])
        if flag == 1: self.SELECTION.append(data[18])
        self.DATA_CNT += 1

    def addChange(self, change):
        self.CHANGES.append(change)

