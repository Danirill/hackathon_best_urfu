# -*- coding: utf-8 -*-
import pandas as pd
from excel.Customer import Custumer
import tkinter as tk
from tkinter import filedialog


def createcsv(filename, flag):

    root = tk.Tk()
    root.withdraw()

    file_path = filedialog.askopenfilename()
    #file = 'data.xlsx'
    excel = pd.ExcelFile(file_path)
    data = excel.parse(excel.sheet_names[0])


    def isBroken(data):
        for item in data:
            if item != item:
                return True
        return False

    def fuckyou(data):
        if data[0] + data[1] + data[2] > 0:
            return 1
        return 0

    customers_dictionary = {}

    for row in range(data.shape[0]):
        key = data.loc[row, :][0]
        if customers_dictionary.get(key) == None:
            customer = Custumer()
            if isBroken(data.loc[row, :]):
                customer.BROKEN = True
            customer.SUBS_ID = key
            customer.Update(data.loc[row, :], flag)
            customers_dictionary[key] = customer
        else:
            if isBroken(data.loc[row, :]):
                customers_dictionary[key].BROKEN = True
            customers_dictionary[key].Update(data.loc[row, :], flag)

    count_of_switched = 0

    for key in customers_dictionary.keys():
        if customers_dictionary[key].DATA_CNT != 3:
            customers_dictionary[key].BROKEN = True

    import csv
    csv_file = ".\output\end" + filename + ".csv"
    try:
        with open(csv_file, 'w') as csvfile:
            writer = csv.writer(csvfile)

            if flag == 1:
                writer.writerow(["CHARGE1", "CHARGE2","CHARGE3",
                                 "TARIFF_ID1", "TARIFF_ID2", "TARIFF_ID3",
                                 "CALLS_OUT_CNT1", "CALLS_OUT_CNT2", "CALLS_OUT_CNT3",
                                 "CALLS_IN_CNT1", "CALLS_IN_CNT2", "CALLS_IN_CNT3",
                                 "DATA_TRAFFIC_MB1", "DATA_TRAFFIC_MB2", "DATA_TRAFFIC_MB3",
                                 "DURATION_IN_MIN1", "DURATION_IN_MIN2", "DURATION_IN_MIN3",
                                 "DURATION_OUT_MIN1", "DURATION_OUT_MIN2", "DURATION_OUT_MIN3",
                                 "LIFE_TIME1", "LIFE_TIME2", "LIFE_TIME3",
                                 "LTE_TRAFF_FLAG1", "LTE_TRAFF_FLAG2", "LTE_TRAFF_FLAG3",
                                 "RECHARGE1", "RECHARGE2", "RECHARGE3",
                                 "RECHARGE_CNT1", "RECHARGE_CNT2", "RECHARGE_CNT3",
                                 "SESSIONS_CNT1", "SESSIONS_CNT2", "SESSIONS_CNT3",
                                 "SMS_IN_CNT1", "SMS_IN_CNT2", "SMS_IN_CNT3",
                                 "SMS_OUT_CNT1", "SMS_OUT_CNT2",
                                 "SMS_OUT_CNT3",
                                 "SUPPORT_3G1", "SUPPORT_3G2", "SUPPORT_3G3",
                                 "SUPPORT_4G1", "SUPPORT_4G2", "SUPPORT_4G3",
                                 "SELECTION"])

                for key in customers_dictionary.keys():
                    if not customers_dictionary[key].BROKEN:
                        writer.writerow([
                                         customers_dictionary[key].CHARGE[0], customers_dictionary[key].CHARGE[1], customers_dictionary[key].CHARGE[2],
                                         customers_dictionary[key].TARIFF_ID[0], customers_dictionary[key].TARIFF_ID[1], customers_dictionary[key].TARIFF_ID[2],
                                         customers_dictionary[key].CALLS_OUT_CNT[0], customers_dictionary[key].CALLS_OUT_CNT[1], customers_dictionary[key].CALLS_OUT_CNT[2],
                                         customers_dictionary[key].CALLS_IN_CNT[0], customers_dictionary[key].CALLS_IN_CNT[1], customers_dictionary[key].CALLS_IN_CNT[2],
                                         customers_dictionary[key].DATA_TRAFFIC_MB[0], customers_dictionary[key].DATA_TRAFFIC_MB[1], customers_dictionary[key].DATA_TRAFFIC_MB[2],
                                         customers_dictionary[key].DURATION_IN_MIN[0], customers_dictionary[key].DURATION_IN_MIN[1], customers_dictionary[key].DURATION_IN_MIN[2],
                                         customers_dictionary[key].DURATION_OUT_MIN[0], customers_dictionary[key].DURATION_OUT_MIN[1], customers_dictionary[key].DURATION_OUT_MIN[2],
                                         customers_dictionary[key].LIFE_TIME[0], customers_dictionary[key].LIFE_TIME[1], customers_dictionary[key].LIFE_TIME[2],
                                         customers_dictionary[key].LTE_TRAFF_FLAG[0], customers_dictionary[key].LTE_TRAFF_FLAG[1], customers_dictionary[key].LTE_TRAFF_FLAG[2],
                                         customers_dictionary[key].RECHARGE[0], customers_dictionary[key].RECHARGE[1], customers_dictionary[key].RECHARGE[2],
                                         customers_dictionary[key].RECHARGE_CNT[0], customers_dictionary[key].RECHARGE_CNT[1], customers_dictionary[key].RECHARGE_CNT[2],
                                         customers_dictionary[key].SESSIONS_CNT[0], customers_dictionary[key].SESSIONS_CNT[1], customers_dictionary[key].SESSIONS_CNT[2],
                                         customers_dictionary[key].SMS_IN_CNT[0], customers_dictionary[key].SMS_IN_CNT[1], customers_dictionary[key].SMS_IN_CNT[2],
                                         customers_dictionary[key].SMS_OUT_CNT[0], customers_dictionary[key].SMS_OUT_CNT[1], customers_dictionary[key].SMS_OUT_CNT[2],
                                         customers_dictionary[key].SUPPORT_3G[0], customers_dictionary[key].SUPPORT_3G[1], customers_dictionary[key].SUPPORT_3G[2],
                                         customers_dictionary[key].SUPPORT_4G[0], customers_dictionary[key].SUPPORT_4G[1], customers_dictionary[key].SUPPORT_4G[2],
                                         fuckyou(customers_dictionary[key].SELECTION)])
            else:


                writer.writerow(["CHARGE1", "CHARGE2", "CHARGE3",
                                 "TARIFF_ID1", "TARIFF_ID2", "TARIFF_ID3",
                                 "CALLS_OUT_CNT1", "CALLS_OUT_CNT2", "CALLS_OUT_CNT3",
                                 "CALLS_IN_CNT1", "CALLS_IN_CNT2", "CALLS_IN_CNT3",
                                 "DATA_TRAFFIC_MB1", "DATA_TRAFFIC_MB2", "DATA_TRAFFIC_MB3",
                                 "DURATION_IN_MIN1", "DURATION_IN_MIN2", "DURATION_IN_MIN3",
                                 "DURATION_OUT_MIN1", "DURATION_OUT_MIN2", "DURATION_OUT_MIN3",
                                 "LIFE_TIME1", "LIFE_TIME2", "LIFE_TIME3",
                                 "LTE_TRAFF_FLAG1", "LTE_TRAFF_FLAG2", "LTE_TRAFF_FLAG3",
                                 "RECHARGE1", "RECHARGE2", "RECHARGE3",
                                 "RECHARGE_CNT1", "RECHARGE_CNT2", "RECHARGE_CNT3",
                                 "SESSIONS_CNT1", "SESSIONS_CNT2", "SESSIONS_CNT3",
                                 "SMS_IN_CNT1", "SMS_IN_CNT2", "SMS_IN_CNT3",
                                 "SMS_OUT_CNT1", "SMS_OUT_CNT2",
                                 "SMS_OUT_CNT3",
                                 "SUPPORT_3G1", "SUPPORT_3G2", "SUPPORT_3G3",
                                 "SUPPORT_4G1", "SUPPORT_4G2", "SUPPORT_4G3",
                                 ])

                for key in customers_dictionary.keys():
                    if not customers_dictionary[key].BROKEN:
                        writer.writerow([
                            customers_dictionary[key].CHARGE[0], customers_dictionary[key].CHARGE[1],
                            customers_dictionary[key].CHARGE[2],
                            customers_dictionary[key].TARIFF_ID[0], customers_dictionary[key].TARIFF_ID[1],
                            customers_dictionary[key].TARIFF_ID[2],
                            customers_dictionary[key].CALLS_OUT_CNT[0], customers_dictionary[key].CALLS_OUT_CNT[1],
                            customers_dictionary[key].CALLS_OUT_CNT[2],
                            customers_dictionary[key].CALLS_IN_CNT[0], customers_dictionary[key].CALLS_IN_CNT[1],
                            customers_dictionary[key].CALLS_IN_CNT[2],
                            customers_dictionary[key].DATA_TRAFFIC_MB[0], customers_dictionary[key].DATA_TRAFFIC_MB[1],
                            customers_dictionary[key].DATA_TRAFFIC_MB[2],
                            customers_dictionary[key].DURATION_IN_MIN[0], customers_dictionary[key].DURATION_IN_MIN[1],
                            customers_dictionary[key].DURATION_IN_MIN[2],
                            customers_dictionary[key].DURATION_OUT_MIN[0], customers_dictionary[key].DURATION_OUT_MIN[1],
                            customers_dictionary[key].DURATION_OUT_MIN[2],
                            customers_dictionary[key].LIFE_TIME[0], customers_dictionary[key].LIFE_TIME[1],
                            customers_dictionary[key].LIFE_TIME[2],
                            customers_dictionary[key].LTE_TRAFF_FLAG[0], customers_dictionary[key].LTE_TRAFF_FLAG[1],
                            customers_dictionary[key].LTE_TRAFF_FLAG[2],
                            customers_dictionary[key].RECHARGE[0], customers_dictionary[key].RECHARGE[1],
                            customers_dictionary[key].RECHARGE[2],
                            customers_dictionary[key].RECHARGE_CNT[0], customers_dictionary[key].RECHARGE_CNT[1],
                            customers_dictionary[key].RECHARGE_CNT[2],
                            customers_dictionary[key].SESSIONS_CNT[0], customers_dictionary[key].SESSIONS_CNT[1],
                            customers_dictionary[key].SESSIONS_CNT[2],
                            customers_dictionary[key].SMS_IN_CNT[0], customers_dictionary[key].SMS_IN_CNT[1],
                            customers_dictionary[key].SMS_IN_CNT[2],
                            customers_dictionary[key].SMS_OUT_CNT[0], customers_dictionary[key].SMS_OUT_CNT[1],
                            customers_dictionary[key].SMS_OUT_CNT[2],
                            customers_dictionary[key].SUPPORT_3G[0], customers_dictionary[key].SUPPORT_3G[1],
                            customers_dictionary[key].SUPPORT_3G[2],
                            customers_dictionary[key].SUPPORT_4G[0], customers_dictionary[key].SUPPORT_4G[1],
                            customers_dictionary[key].SUPPORT_4G[2]])
    except IOError:
        print("I/O error")
