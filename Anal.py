import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense
from sklearn.model_selection import train_test_split

import tkinter as tk
from tkinter import filedialog

root = tk.Tk()
root.withdraw()

file_path = filedialog.askopenfilename()

np.random.seed(42)
df = pd.read_csv(file_path)
df.head()

feat = df.drop(columns=['SELECTION'], axis = 1)
label = df["SELECTION"]
x_train, x_test, y_train, y_test = train_test_split(feat, label, test_size=0.2, random_state=32)

model = Sequential()
model.add(Dense(128, activation ='relu', input_shape = (x_train.shape[1],)))
model.add(Dense(1))

model.compile(optimizer ='adam', loss='mse',metrics=['mae'])

model.fit(x_train, y_train, epochs=100, batch_size=1, verbose=1)

# Генерируем описание модели в формате json
model_json = model.to_json()
# Записываем модель в файл
json_file = open(".\output\hackathon_model.json", "w")
json_file.write(model_json)
json_file.close()
#Сохранение весов
model.save_weights(".\output\hackathon_weights.h5")









