import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.models import model_from_json
import numpy
import os
from keras.preprocessing import image
import pandas as pd
import tkinter as tk
from tkinter import filedialog
from excel.Reader import createcsv

createcsv('checker', 0)


df = pd.read_csv('.\output\endchecker.csv')


with open('.\output\hackathon_model.json','r') as f:
    json = f.read()
loaded_model = model_from_json(json)

#Весы
loaded_model.load_weights(".\output\hackathon_weights.h5")

pred = loaded_model.predict(df)
print(pred)